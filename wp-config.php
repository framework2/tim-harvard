<?php
// Set your environment/url pairs
$environments = array(
    'tim-harvard.dev'   => 'local',
    'tim-harvard.test'  => 'local',
    'tim-harvard.wtframework.co.uk' => 'development',
    'tim-harvard.com' => 'production'
);
// define('ENVIRONMENT', 'production');

// Get the hostname
$http_host = $_SERVER['HTTP_HOST'];

// Loop through $environments to see if there’s a match
foreach($environments as $hostname => $environment) {
  if (stripos($http_host, $hostname) !== FALSE) {
    define('ENVIRONMENT', $environment);
    break;
  }
}

if (!defined('ENVIRONMENT')){
	 define('ENVIRONMENT', 'production');
}

if (!defined('ENVIRONMENT')) exit('No database configured for this host');

// Location of environment-specific configuration
$wp_db_config = 'wp-config/wp-db-' . ENVIRONMENT . '.php';

// Check to see if the configuration file for the environment exists
if (file_exists(__DIR__ . '/' . $wp_db_config)) {
  require_once($wp_db_config);
} else {
  // Exit if configuration file does not exist
  exit('No database configuration found for this host');
}

/* That’s all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

define( 'WP_POST_REVISIONS', 4 );


/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
